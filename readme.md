
### SPR-KKR hands-on course for magnetism and spectroscopy

22--25 November 2021

- to be used with [Oracle VirtualBox v6.1.6 or later](https://www.virtualbox.org/)
- "import" ova file into Virtual Box
- based on [Quantum-Mobile version 21.06.04](https://quantum-mobile.readthedocs.io/en/latest/releases/versions/21.06.04.html)
- virtual machine with [SPR-KKR and Xband version 8.6](https://www.ebert.cup.uni-muenchen.de/old/index.php?option=com_content&view=article&id=8%3Asprkkr&catid=4%3Asoftware&Itemid=7&lang=de)
- ase2sprkkr version 0.2.0

